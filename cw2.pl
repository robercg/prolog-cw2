%Group 24
%960823 Chris Humble
%958181 James Hodder
%969197 Roberto Carrillo Granados
%All the code in this assignment is the group's own work


%Q1
parent(queenmother,elisabeth).
parent(elisabeth,charles).
parent(elisabeth,andrew).
parent(elisabeth,anne).
parent(elisabeth,edward).
parent(diana,william).
parent(diana,harry).
parent(sarah,beatrice).
parent(anne,peter).
parent(anne,zara).
parent(george,elisabeth).
parent(philip,charles).
parent(philip,andrew).
parent(philip,edward).
parent(charles,william).
parent(charles,harry).
parent(andrew,beatrice).
parent(andrew,eugenie).
parent(mark,peter).
parent(mark,zara).
parent(william,georgejun).
parent(kate,georgejun).
parent(william,charlotte).
parent(kate,charlotte).
parent(philip,anne).
parent(william,louis).
parent(kate,louis).

%Q1iA
the_royal_females([queenmother,elisabeth,anne,diana,sarah,beatrice, zara, kate, charlotte, eugenie]).

%Q1iB
the_royal_males([charles,andrew,edward,william,harry,peter,george,philip,mark,georgejun,louis]).

%Q1ii
the_royal_family(X):-
    the_royal_males(A),
    the_royal_females(B),
    append(A,B,X).

%Q1iii
mother(X, Y):-
    the_royal_females(A),
    member(X,A),
    parent(X,Y).

%Q1iv
ancestor(X,Y):-
    parent(X,Y).

ancestor(X,Y):-
    parent(X,Z),
    ancestor(Z,Y).

%Q1v
sibling(X,Y):-
    parent(Z,X),
    parent(Z,Y),
	X \== Y.

%Q1vi
brother(X,Y):-
    sibling(X,Y),
    the_royal_males(A),
    member(X,A).

%Q1vii
%william, kate, queenmother, elisabeth, diana, george, philip and charles are ancestors of louis.

%Q1viii
grandmother(X,Y):-
    mother(X,Z),
    parent(Z,Y).
%charles, andrew, anne and edward are grandchildren of queenmother.

%Q1xi
common_ancestors(X,Y,Z):-
    ancestor(X,Z),
    ancestor(Y,Z).
%there are no common decendants, as edward has no children

%Q1x
has_brother_who_is_granddad(X,Y):-
    brother(Y,X),
	parent(Y,Z),
    parent(Z,A).
%andrew, edward and anne all have a brother who is a grand dad (the brother is charles for all 3)

%Q2A
nth_elt(N,L,E,R):-
    nth1(N,L,E),
    delete(L,E,R).

nth_elt_with_test(N,L,E,R):- N<0, length(L,LX), LX > N,!.
nth_elt_with_test(N,L,E,R):- nth_elt(N,L,E,R).

%Q2B
order([H|T],R):- order(T,TR), orderRec(H,TR,R).

orderRec(H,[],[H]):-!.
orderRec(H,[X|Y],[H,X|Y]):- H < X,!.
orderRec(H,[X|Y],[X|Z]):-  orderRec(H,Y,Z),!.


%Q2C

median(L,M) :- order(L,R), length(R, Lth), LthMod is Lth mod 2,
    LthMod == 1 -> P is round(Lth+1)/2, nth1(P,R,M),!.
median(L,M) :- order(L,R), length(R, Lth), LthMod is Lth mod 2,
    LthMod == 0 ->	P1 is (Lth)/2, P2 is P1+1, nth1(P2,R,M1), nth1(P1,R,M2), M is (M1+M2)/2.

%Q3A
euclidsqr([],[],0).
euclidsqr([A|A1],[B|B1],ED):-
    euclidsqr(A1,B1,ED1),
    ED is ED1 + (A - B) * (A - B).

%Q3B
euclidsqr_acc([],[],Acc,Acc).
euclidsqr_acc([A|A1],[B|B1],OldAcc,ED):-
    NewAcc is OldAcc + (A - B) * (A - B),
    euclidsqr_acc(A1,B1,NewAcc,ED).

%Q4A
member_rem(E,[L1 |_],R):- not(member(L1, R)), E is L1.
member_rem(E,[_ |LS],R):- member_rem(E, LS, R).

%Q4B

genR(0, L, _, L).
genR(N, L,D , O):-
    N2 is N-1,
    member_rem(E, D, L),
    append([E], L, L2),
    genR(N2, L2, D, O).

gen_list(N, L, D):- length(D, DL), DL >=N, genR(N, [], D, L).

%Q4C

solve_sujiko1([X1, X2, X3, X4, X5, X6, X7]):-
    gen_list(7,[X1, X2, X3, X4, X5, X6, X7],[1,2,3,4,5,6,7,8,9]),
	not(member(2,[X1, X2, X3, X4, X5, X6, X7])),
	not(member(7,[X1, X2, X3, X4, X5, X6, X7])),
	23 is X1 + X2 + X4 + X5,
	27 is X2 + X3 + X5 + X6,
	17 is X4 + X5 + 7 + X7,
	17 is X5 + X6 + X7 + 2.


solve_sujiko2([X1, X2, X3, X4, X5, X6, X7]):-
    gen_list(7,[X1, X2, X3, X4, X5, X6, X7],[1,2,3,4,5,6,7,8,9]),
    not(member(5,[X1, X2, X3, X4, X5, X6, X7])),
    not(member(7,[X1, X2, X3, X4, X5, X6, X7])),
	21 is X1 + X2 + 5 + X3,
	22 is X2 + 7 + X3 + X4,
	24 is 5 + X3 + X5 + X6,
	14 is X3 + X4 + X6 + X7.

%Q4D

member_rem_list(AS, AS, [], _).
member_rem_list(FS, AS,[L | LS],RS):-
    not(member(L, RS)),
    append([L],AS,AS2),!,
    member_rem_list(FS, AS2, LS, RS).
member_rem_list(FS, AS, [_ | LS], RS):-
    member_rem_list(FS, AS, LS, RS).




calcPossibilities([A,B,C,D],[X1,X2,X3,X4,X5,X6,X7,X8,X9]):-
    gen_list(4, [X1,X2,X4,X5],[1,2,3,4,5,6,7,8,9]),
    A is X1+X2+X4+X5,
    member_rem_list(PS, [], [1,2,3,4,5,6,7,8,9],[X1, X2, X4, X5]),
    gen_list(2, [X3, X6], PS),
    B is X2 + X3 + X5 + X6,
    member_rem_list(PS2, [], PS, [X3, X6]),
    gen_list(2, [X7, X8], PS2),
    C is X4+X5+X7+X8,
    member_rem_list(PS3, [], PS2, [X7, X8]),
    gen_list(1, [X9], PS3),
    D is X5+X6+X8+X9.


new_solve_sujiko2([X11,X12,X22,X23,X31,X32,X33]):-
	calcPossibilities([21,22,24,14], [X11,X12,7,5,X22,X23,X31,X32,X33]).


